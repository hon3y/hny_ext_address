<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_address', 'Configuration/TypoScript', 'hive_ext_address');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextaddress_domain_model_address', 'EXT:hive_ext_address/Resources/Private/Language/locallang_csh_tx_hiveextaddress_domain_model_address.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextaddress_domain_model_address');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextaddress_domain_model_coordinate', 'EXT:hive_ext_address/Resources/Private/Language/locallang_csh_tx_hiveextaddress_domain_model_coordinate.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextaddress_domain_model_coordinate');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextaddress_domain_model_city', 'EXT:hive_ext_address/Resources/Private/Language/locallang_csh_tx_hiveextaddress_domain_model_city.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextaddress_domain_model_city');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextaddress_domain_model_stateprovince', 'EXT:hive_ext_address/Resources/Private/Language/locallang_csh_tx_hiveextaddress_domain_model_stateprovince.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextaddress_domain_model_stateprovince');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextaddress_domain_model_country', 'EXT:hive_ext_address/Resources/Private/Language/locallang_csh_tx_hiveextaddress_domain_model_country.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextaddress_domain_model_country');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextaddress_domain_model_zip', 'EXT:hive_ext_address/Resources/Private/Language/locallang_csh_tx_hiveextaddress_domain_model_zip.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextaddress_domain_model_zip');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextaddress_domain_model_region', 'EXT:hive_ext_address/Resources/Private/Language/locallang_csh_tx_hiveextaddress_domain_model_region.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextaddress_domain_model_region');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder