## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveextaddress {

        persistence {
            storagePid =
        }

        model {
            HIVE\HiveExtAddress\Domain\Model\Address {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtAddress\Domain\Model\City {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtAddress\Domain\Model\StateProvince {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtAddress\Domain\Model\Country {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtAddress\Domain\Model\Region {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtAddress\Domain\Model\Zip {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtAddress\Domain\Model\Coordinate {
                persistence {
                    storagePid =
                }
            }
        }
    }
}