<?php

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.svg';

/*
 * Backend
 */
$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'uid';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'title, street, nr, coordinate, city';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

$GLOBALS['TCA'][$sModel]['ctrl']['default_sortby'] = 'ORDER BY title ASC, street ASC';

/*
 * Coordinate
 */
$sColumn = 'coordinate';
$sTable = 'tx_hiveextaddress_domain_model_coordinate';
$sUserFuncModel = 'HIVE\\HiveExtAddress\\Domain\\Model\\Coordinate';
$GLOBALS['TCA'][$sModel]['columns']['coordinate']['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
//    'MM' => 'sys_file_metadata_pages_mm',
    'size' => 1,
    'maxitems' => 1,
    'multiple' => 0,
    'fieldControl' => [
        'addRecord' => [
            'disabled' => 0,
            'options' => [
                'setValue' => 'append'
            ],
        ],
    ],
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];
$sPidList = (string) HIVE\HiveExtAddress\UserFunc\StorageUserFunc::getStoragePidListForModel($sUserFuncModel);
if ($sPidList != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['suggestOptions']['default']['pidList'] = $sPidList;
}
$sFirstPid = (string) HIVE\HiveExtAddress\UserFunc\StorageUserFunc::getFirstStoragePidForModel($sUserFuncModel);
if ($sFirstPid != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['fieldControl']['addRecord']['options']['pid'] = $sFirstPid;
}

/*
 * City
 */
$sColumn = 'city';
$sTable = 'tx_hiveextaddress_domain_model_city';
$sUserFuncModel = 'HIVE\\HiveExtAddress\\Domain\\Model\\City';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
    'size' => 1,
    'maxitems' => 1,
    'multiple' => 0,
    'fieldControl' => [
        'addRecord' => [
            'disabled' => 0,
            'options' => [
                'setValue' => 'append'
            ],
        ],
    ],
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];
$sPidList = (string) HIVE\HiveExtAddress\UserFunc\StorageUserFunc::getStoragePidListForModel($sUserFuncModel);
if ($sPidList != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['suggestOptions']['default']['pidList'] = $sPidList;
}
$sFirstPid = (string) HIVE\HiveExtAddress\UserFunc\StorageUserFunc::getFirstStoragePidForModel($sUserFuncModel);
if ($sFirstPid != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['fieldControl']['addRecord']['options']['pid'] = $sFirstPid;
}

/*
 * Zip
 */
$sColumn = 'zip';
$sTable = 'tx_hiveextaddress_domain_model_zip';
$sUserFuncModel = 'HIVE\\HiveExtAddress\\Domain\\Model\\Zip';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
    'size' => 1,
    'maxitems' => 1,
    'multiple' => 0,
    'fieldControl' => [
        'addRecord' => [
            'disabled' => 0,
            'options' => [
                'setValue' => 'append'
            ],
        ],
    ],
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];
$sPidList = (string) HIVE\HiveExtAddress\UserFunc\StorageUserFunc::getStoragePidListForModel($sUserFuncModel);
if ($sPidList != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['suggestOptions']['default']['pidList'] = $sPidList;
}
$sFirstPid = (string) HIVE\HiveExtAddress\UserFunc\StorageUserFunc::getFirstStoragePidForModel($sUserFuncModel);
if ($sFirstPid != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['fieldControl']['addRecord']['options']['pid'] = $sFirstPid;
}
