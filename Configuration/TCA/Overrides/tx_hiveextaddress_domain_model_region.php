<?php

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/PNG/hive_16x16.png';

$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'uid';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'title';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

$GLOBALS['TCA'][$sModel]['ctrl']['default_sortby'] = 'ORDER BY title ASC';