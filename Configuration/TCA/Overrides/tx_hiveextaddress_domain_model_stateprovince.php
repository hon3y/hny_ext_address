<?php

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');

$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/PNG/hive_16x16.png';

$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'uid';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'title, country';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

$GLOBALS['TCA'][$sModel]['ctrl']['default_sortby'] = 'ORDER BY title ASC';

/*
 * Country
 */
$sColumn = 'country';
$sTable = 'tx_hiveextaddress_domain_model_country';
$sUserFuncModel = 'HIVE\\HiveExtAddress\\Domain\\Model\\Country';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
    'size' => 1,
    'maxitems' => 1,
    'multiple' => 0,
    'fieldControl' => [
        'addRecord' => [
            'disabled' => 0,
            'options' => [
                'setValue' => 'append'
            ],
        ],
    ],
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];
$sPidList = (string) HIVE\HiveExtAddress\UserFunc\StorageUserFunc::getStoragePidListForModel($sUserFuncModel);
if ($sPidList != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['suggestOptions']['default']['pidList'] = $sPidList;
}
$sFirstPid = (string) HIVE\HiveExtAddress\UserFunc\StorageUserFunc::getFirstStoragePidForModel($sUserFuncModel);
if ($sFirstPid != '') {
    $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['fieldControl']['addRecord']['options']['pid'] = $sFirstPid;
}