<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hive_ext_address/Resources/Private/Language/locallang_db.xlf:tx_hiveextaddress_domain_model_address',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,street,nr,addon,coordinate,city,zip',
        'iconfile' => 'EXT:hive_ext_address/Resources/Public/Icons/tx_hiveextaddress_domain_model_address.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, street, nr, addon, coordinate, city, zip',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, street, nr, addon, coordinate, city, zip, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hiveextaddress_domain_model_address',
                'foreign_table_where' => 'AND tx_hiveextaddress_domain_model_address.pid=###CURRENT_PID### AND tx_hiveextaddress_domain_model_address.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_address/Resources/Private/Language/locallang_db.xlf:tx_hiveextaddress_domain_model_address.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'street' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_address/Resources/Private/Language/locallang_db.xlf:tx_hiveextaddress_domain_model_address.street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'nr' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_address/Resources/Private/Language/locallang_db.xlf:tx_hiveextaddress_domain_model_address.nr',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'addon' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_address/Resources/Private/Language/locallang_db.xlf:tx_hiveextaddress_domain_model_address.addon',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'coordinate' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_address/Resources/Private/Language/locallang_db.xlf:tx_hiveextaddress_domain_model_address.coordinate',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_hiveextaddress_domain_model_coordinate',
                'minitems' => 0,
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'city' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_address/Resources/Private/Language/locallang_db.xlf:tx_hiveextaddress_domain_model_address.city',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_hiveextaddress_domain_model_city',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'zip' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_address/Resources/Private/Language/locallang_db.xlf:tx_hiveextaddress_domain_model_address.zip',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_hiveextaddress_domain_model_zip',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
    
    ],
];
