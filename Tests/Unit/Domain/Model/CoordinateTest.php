<?php
namespace HIVE\HiveExtAddress\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class CoordinateTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtAddress\Domain\Model\Coordinate
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtAddress\Domain\Model\Coordinate();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLatReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getLat()
        );
    }

    /**
     * @test
     */
    public function setLatForFloatSetsLat()
    {
        $this->subject->setLat(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'lat',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getLonReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getLon()
        );
    }

    /**
     * @test
     */
    public function setLonForFloatSetsLon()
    {
        $this->subject->setLon(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'lon',
            $this->subject,
            '',
            0.000000001
        );
    }
}
