<?php
namespace HIVE\HiveExtAddress\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class AddressTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtAddress\Domain\Model\Address
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtAddress\Domain\Model\Address();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStreetReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getStreet()
        );
    }

    /**
     * @test
     */
    public function setStreetForStringSetsStreet()
    {
        $this->subject->setStreet('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'street',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getNrReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getNr()
        );
    }

    /**
     * @test
     */
    public function setNrForStringSetsNr()
    {
        $this->subject->setNr('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'nr',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAddonReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAddon()
        );
    }

    /**
     * @test
     */
    public function setAddonForStringSetsAddon()
    {
        $this->subject->setAddon('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'addon',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCoordinateReturnsInitialValueForCoordinate()
    {
        self::assertEquals(
            null,
            $this->subject->getCoordinate()
        );
    }

    /**
     * @test
     */
    public function setCoordinateForCoordinateSetsCoordinate()
    {
        $coordinateFixture = new \HIVE\HiveExtAddress\Domain\Model\Coordinate();
        $this->subject->setCoordinate($coordinateFixture);

        self::assertAttributeEquals(
            $coordinateFixture,
            'coordinate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForCity()
    {
        self::assertEquals(
            null,
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForCitySetsCity()
    {
        $cityFixture = new \HIVE\HiveExtAddress\Domain\Model\City();
        $this->subject->setCity($cityFixture);

        self::assertAttributeEquals(
            $cityFixture,
            'city',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZipReturnsInitialValueForZip()
    {
        self::assertEquals(
            null,
            $this->subject->getZip()
        );
    }

    /**
     * @test
     */
    public function setZipForZipSetsZip()
    {
        $zipFixture = new \HIVE\HiveExtAddress\Domain\Model\Zip();
        $this->subject->setZip($zipFixture);

        self::assertAttributeEquals(
            $zipFixture,
            'zip',
            $this->subject
        );
    }
}
