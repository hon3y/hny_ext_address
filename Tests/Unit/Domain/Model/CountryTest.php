<?php
namespace HIVE\HiveExtAddress\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class CountryTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtAddress\Domain\Model\Country
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtAddress\Domain\Model\Country();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIso3166Alpha2ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIso3166Alpha2()
        );
    }

    /**
     * @test
     */
    public function setIso3166Alpha2ForStringSetsIso3166Alpha2()
    {
        $this->subject->setIso3166Alpha2('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'iso3166Alpha2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getIso3166Alpha3ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getIso3166Alpha3()
        );
    }

    /**
     * @test
     */
    public function setIso3166Alpha3ForStringSetsIso3166Alpha3()
    {
        $this->subject->setIso3166Alpha3('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'iso3166Alpha3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRegionReturnsInitialValueForRegion()
    {
        self::assertEquals(
            null,
            $this->subject->getRegion()
        );
    }

    /**
     * @test
     */
    public function setRegionForRegionSetsRegion()
    {
        $regionFixture = new \HIVE\HiveExtAddress\Domain\Model\Region();
        $this->subject->setRegion($regionFixture);

        self::assertAttributeEquals(
            $regionFixture,
            'region',
            $this->subject
        );
    }
}
