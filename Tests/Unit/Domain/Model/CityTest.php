<?php
namespace HIVE\HiveExtAddress\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 */
class CityTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtAddress\Domain\Model\City
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtAddress\Domain\Model\City();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStateProvinceReturnsInitialValueForStateProvince()
    {
        self::assertEquals(
            null,
            $this->subject->getStateProvince()
        );
    }

    /**
     * @test
     */
    public function setStateProvinceForStateProvinceSetsStateProvince()
    {
        $stateProvinceFixture = new \HIVE\HiveExtAddress\Domain\Model\StateProvince();
        $this->subject->setStateProvince($stateProvinceFixture);

        self::assertAttributeEquals(
            $stateProvinceFixture,
            'stateProvince',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZipReturnsInitialValueForZip()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getZip()
        );
    }

    /**
     * @test
     */
    public function setZipForObjectStorageContainingZipSetsZip()
    {
        $zip = new \HIVE\HiveExtAddress\Domain\Model\Zip();
        $objectStorageHoldingExactlyOneZip = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneZip->attach($zip);
        $this->subject->setZip($objectStorageHoldingExactlyOneZip);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneZip,
            'zip',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addZipToObjectStorageHoldingZip()
    {
        $zip = new \HIVE\HiveExtAddress\Domain\Model\Zip();
        $zipObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $zipObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($zip));
        $this->inject($this->subject, 'zip', $zipObjectStorageMock);

        $this->subject->addZip($zip);
    }

    /**
     * @test
     */
    public function removeZipFromObjectStorageHoldingZip()
    {
        $zip = new \HIVE\HiveExtAddress\Domain\Model\Zip();
        $zipObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $zipObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($zip));
        $this->inject($this->subject, 'zip', $zipObjectStorageMock);

        $this->subject->removeZip($zip);
    }
}
