<?php
namespace HIVE\HiveExtAddress\Controller;

/***
 *
 * This file is part of the "hive_ext_address" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * CoordinateController
 */
class CoordinateController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * coordinateRepository
     *
     * @var \HIVE\HiveExtAddress\Domain\Repository\CoordinateRepository
     * @inject
     */
    protected $coordinateRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $coordinates = $this->coordinateRepository->findAll();
        $this->view->assign('coordinates', $coordinates);
    }
}
