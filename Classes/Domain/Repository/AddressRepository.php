<?php
namespace HIVE\HiveExtAddress\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_address" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Addresses
 */
class AddressRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtAddress\\Domain\\Model\\Address';
        $sUserFuncPlugin = 'tx_hiveextaddress';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
