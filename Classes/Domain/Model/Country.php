<?php
namespace HIVE\HiveExtAddress\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_address" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * Country
 */
class Country extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * iso3166Alpha2
     *
     * @var string
     * @validate NotEmpty
     */
    protected $iso3166Alpha2 = '';

    /**
     * iso3166Alpha3
     *
     * @var string
     * @validate NotEmpty
     */
    protected $iso3166Alpha3 = '';

    /**
     * region
     *
     * @var \HIVE\HiveExtAddress\Domain\Model\Region
     */
    protected $region = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the iso3166Alpha2
     *
     * @return string $iso3166Alpha2
     */
    public function getIso3166Alpha2()
    {
        return $this->iso3166Alpha2;
    }

    /**
     * Sets the iso3166Alpha2
     *
     * @param string $iso3166Alpha2
     * @return void
     */
    public function setIso3166Alpha2($iso3166Alpha2)
    {
        $this->iso3166Alpha2 = $iso3166Alpha2;
    }

    /**
     * Returns the iso3166Alpha3
     *
     * @return string $iso3166Alpha3
     */
    public function getIso3166Alpha3()
    {
        return $this->iso3166Alpha3;
    }

    /**
     * Sets the iso3166Alpha3
     *
     * @param string $iso3166Alpha3
     * @return void
     */
    public function setIso3166Alpha3($iso3166Alpha3)
    {
        $this->iso3166Alpha3 = $iso3166Alpha3;
    }

    /**
     * Returns the region
     *
     * @return \HIVE\HiveExtAddress\Domain\Model\Region $region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Sets the region
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Region $region
     * @return void
     */
    public function setRegion(\HIVE\HiveExtAddress\Domain\Model\Region $region)
    {
        $this->region = $region;
    }
}
