<?php
namespace HIVE\HiveExtAddress\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_address" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Address
 */
class Address extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * street
     *
     * @var string
     * @validate NotEmpty
     */
    protected $street = '';

    /**
     * nr
     *
     * @var string
     * @validate NotEmpty
     */
    protected $nr = '';

    /**
     * addon
     *
     * @var string
     * @validate NotEmpty
     */
    protected $addon = '';

    /**
     * coordinate
     *
     * @var \HIVE\HiveExtAddress\Domain\Model\Coordinate
     */
    protected $coordinate = null;

    /**
     * city
     *
     * @var \HIVE\HiveExtAddress\Domain\Model\City
     */
    protected $city = null;

    /**
     * zip
     *
     * @var \HIVE\HiveExtAddress\Domain\Model\Zip
     */
    protected $zip = null;

    /**
     * Returns the street
     *
     * @return string $street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Sets the street
     *
     * @param string $street
     * @return void
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * Returns the nr
     *
     * @return string $nr
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * Sets the nr
     *
     * @param string $nr
     * @return void
     */
    public function setNr($nr)
    {
        $this->nr = $nr;
    }

    /**
     * Returns the addon
     *
     * @return string $addon
     */
    public function getAddon()
    {
        return $this->addon;
    }

    /**
     * Sets the addon
     *
     * @param string $addon
     * @return void
     */
    public function setAddon($addon)
    {
        $this->addon = $addon;
    }

    /**
     * Returns the coordinate
     *
     * @return \HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinate
     */
    public function getCoordinate()
    {
        return $this->coordinate;
    }

    /**
     * Sets the coordinate
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinate
     * @return void
     */
    public function setCoordinate(\HIVE\HiveExtAddress\Domain\Model\Coordinate $coordinate)
    {
        $this->coordinate = $coordinate;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {

    }

    /**
     * Returns the city
     *
     * @return \HIVE\HiveExtAddress\Domain\Model\City $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\City $city
     * @return void
     */
    public function setCity(\HIVE\HiveExtAddress\Domain\Model\City $city)
    {
        $this->city = $city;
    }

    /**
     * Returns the zip
     *
     * @return \HIVE\HiveExtAddress\Domain\Model\Zip $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Zip $zip
     * @return void
     */
    public function setZip(\HIVE\HiveExtAddress\Domain\Model\Zip $zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the title
     *
     * @return string title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}
