<?php
namespace HIVE\HiveExtAddress\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_address" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * StateProvince
 */
class StateProvince extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * country
     *
     * @var \HIVE\HiveExtAddress\Domain\Model\Country
     */
    protected $country = null;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the country
     *
     * @return \HIVE\HiveExtAddress\Domain\Model\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param \HIVE\HiveExtAddress\Domain\Model\Country $country
     * @return void
     */
    public function setCountry(\HIVE\HiveExtAddress\Domain\Model\Country $country)
    {
        $this->country = $country;
    }
}
